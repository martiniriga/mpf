var Module = {} || Module;

Module.GetTasks = (function () {
    var pub = {},
        _userId,            //userID of current user
        _tasks = [],        //List of our tasks
        _options = {
            listName: "myTasks",                  //Name of list we want
            container: "#myTasks",    //id of html element we're rendering our list of tasks in
        };

    //Module Initializer
    pub.init = function () {
        var clientContext = new SP.ClientContext.get_current();
        _userId = clientContext.get_web().get_currentUser();
        clientContext.load(_userId);
        clientContext.executeQueryAsync(getUserInfo, _onQueryFailed);

    };


    //Once we have our userId, we make Asyncronous call to get our list defined by _options.listName
    function getUserInfo() {
        _userId = _userId.get_id();

        getSpecifiedList(_options.listName, _userId);
    }

    //Makes a REST Call to grab a specified List with items assigned to the userId. Items must not have a status of 'Completed'
    function getSpecifiedList(listName, userId) {
        var url = _spPageContextInfo.webAbsoluteUrl + "/_api/lists/getbytitle('" + listName + "')/items?$filter=(AssignedTo eq '" + userId + "') and (Status ne 'Completed')";
        $.ajax({
            url: url,
            type: "GET",
            headers: {
                "accept": "application/json;odata=verbose",
            },
            success: function (results) { createTaskView(results, listName); },
            error: function (error) {
                console.log("Error in getting List: " + listName);
                $(_options.container).html("Error retrieving your " + listName + ".");
            }
        });
    }

    //Upon Receiving Task List data, we set as our module's _task list .
    //We then iterate through each task and add to a table which we then insert into our container defined in _options.container
    function createTaskView(results, listName) {
        _tasks = results.d.results;

        var table = $("<div class='row'>" +
                                "<div class='col-md-4'>Title</div>" +
                                "<div class='col-md-4'>Due Date</div>" +
                                "<div class='col-md-4'>Status</div>" +
                      "</div>");
        $.each(_tasks, function (index, task) {
            var tr = "<div class='row'>" +
                       "<div class='col-md-4'>" + task.Title + "</div>" +
                       "<div class='col-md-4'>" + new Date(task.DueDate).toLocaleDateString() + "</div>" +
                       "<div class='col-md-4'>" + task.Status + "</div>" +
                    "</div>";
            table.append(tr);
        });
        $(_options.container).html(table);
    }

    function _onQueryFailed(sender, args) {
        alert('Request failed. \nError: ' + args.get_message() + '\nStackTrace: ' + args.get_stackTrace());
    }

    return pub;
}());

$(document).ready(function () {
    //must wait for SP scripts as we require them in our code
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        Module.GetTasks.init();
    });
    //or simply
    $.ajax({
        url: "/sites/mpf/_api/lists/getbytitle('myTasks')/Items",
        method: 'GET',
        beforeSend: function (XMLHttpRequest) {

            XMLHttpRequest.setRequestHeader("Accept", "application/json; odata=verbose");
        },

        cache: true,
        error: function (data) {
        },

        success: function (data) {
            console.log(data.d.results);
        }
    });


});

