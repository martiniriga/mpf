<?php
$username = "masterp1_user";
$password = "masterpiece*2015";
$pdo = new PDO('mysql:host=localhost;dbname=masterp1_journal;', $username, $password);		
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(isset($_POST['btnSubmit'])):

	
	try {
		$userid = "";
		$tskLeo =  $_POST["tskLeo"];
		$tskKesho =$_POST["tskKesho"];
		$email =$_POST["email"];
		$fullnames=  explode("@", $email);
		$fullnames = $fullnames[0];
		$leoRows= count($tskLeo);
		$keshoRows= count($tskKesho);
		$rowct = "";
		if($leoRows > $keshoRows)
		{
			$rowct = $leoRows;
		}
		else
		{
			$rowct = $keshoRows;
		}


		$stmt = $pdo->prepare("SELECT id FROM user WHERE email=?");
		$stmt->execute(array($email));			
		$rows = $stmt->rowCount();
		if($rows>0)
		{
			$userid = $stmt->fetchColumn(); 
		}
		else
		{ //user doesn't exist
		die(json_encode(array('msg' => 'Account '.$fullnames.', does not exist','type'=>'warning')));
		}



			//check if activities exists
	$stmt = $pdo->prepare("SELECT count(*) FROM activity  WHERE userid=? AND date=?");
	$stmt->execute(array($userid,date("Y-m-d")));
	$rows = $stmt->fetchColumn();
	if($rows>0)
	{
		echo json_encode(array('msg' => $fullnames.', you have already submitted your journal for the day','type'=>'warning'));
	}			
	else
	{	
		$result =array();
		for ($i=0; $i < $rowct ; $i++) { 
			$stmt = $pdo->prepare('INSERT INTO activity(userid,tskLeo,tskKesho,date) VALUES(?,?,?,?)');
			if(!isset($tskLeo[$i]))
				$tskLeo[$i]="";
			if(!isset($tskKesho[$i]))
				$tskKesho[$i]="";
			if($stmt->execute(array( $userid,$tskLeo[$i],$tskKesho[$i],date("Y-m-d") )))
			{
				$rowid[] = $pdo->lastInsertId();
				$result['fullnames'] = $fullnames;
				$result['tskLeo'] = $tskLeo;
				$result['tskKesho'] = $tskKesho;
				$result['rowid'] = $rowid;
				if($rowct-1 || $rowct=1){
					$result['type'] ='success';
					$result['msg'] = $fullnames.' recorded successfully';
					$result['userid'] = $userid;
					$result['email'] = $email;
				}

			}	
		}
		echo json_encode($result);

	}


} 
catch(PDOException $e)
{
	echo json_encode(array('type'=>'error','msg' => ($e->getMessage() ) ));
}

endif;
if(isset($_POST['btnUpdate'])):
	$tskLeo =  $_POST["tskLeo"];
	$tskKesho =$_POST["tskKesho"];
	$email =$_POST["email"];
	$fullnames=  explode("@", $email);
	$fullnames = $fullnames[0];
	$leoRows= count($tskLeo);
	$keshoRows= count($tskKesho);
	$rowct = "";
	if($leoRows > $keshoRows)
	{
		$rowct = $leoRows;
	}
	else
	{
		$rowct = $keshoRows;
	}

	$stmt = $pdo->prepare("SELECT id FROM user WHERE email=?");
	$stmt->execute(array($email));			
	$rows = $stmt->rowCount();
	if($rows>0)
	{
		$userid = $stmt->fetchColumn(); 
	}
	else
	{ //user doesn't exist
		die(json_encode(array('msg' => 'Account '.$fullnames.', does not exist','type'=>'warning')));
	}	
	$rowid = explode(",", $_POST['rowid']);
	$idcount = count($rowid);

	$result =array();
	for ($i=0; $i < $idcount ; $i++) 
	{ 
		if(!isset($tskLeo[$i])) $tskLeo[$i]="";
		if(!isset($tskKesho[$i])) $tskKesho[$i]="";

		$stmt = $pdo->prepare('UPDATE activity set userid=:userid, tskLeo=:tskLeo,tskKesho=:tskKesho WHERE ID=:id');
		if($stmt->execute(array( ':userid'=> $userid,':tskLeo'=> $tskLeo[$i],':tskKesho'=> $tskKesho[$i],':id' =>$rowid[$i] )))
		{
			$result['fullnames'] = $fullnames;
			$result['tskLeo'] = $tskLeo;
			$result['tskKesho'] = $tskKesho;
			if($i==$idcount-1){
					$result['type'] ='success';
					$result['msg'] = $fullnames.' recorded successfully';
					$result['update'] = 'update';
				}
		}	
		else
		{
			$result['type'] ='error';
			$result['msg'] = $stmt->errorInfo();
		}
	}
	if ($rowct>$idcount) {
		for ($i=$idcount; $i < $rowct ; $i++) { 
			$stmt = $pdo->prepare('INSERT INTO activity(userid,tskLeo,tskKesho,date) VALUES(?,?,?,?)');
			if(!isset($tskLeo[$i]))
				$tskLeo[$i]="";
			if(!isset($tskKesho[$i]))
				$tskKesho[$i]="";
			if($stmt->execute(array( $userid,$tskLeo[$i],$tskKesho[$i],date("Y-m-d") )))
			{

				$result['fullnames'] = $fullnames;
				$result['tskLeo'] = $tskLeo;
				$result['tskKesho'] = $tskKesho;
				if($i==$rowct-1 || $rowct==1){
					$result['type'] ='success';
					$result['msg'] = $fullnames.' recorded successfully';
				}

			}	
		}
	}
	echo json_encode($result);

endif;
?>
