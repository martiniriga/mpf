<?php
$username = "masterp1_user";
$password = "masterpiece*2015";
$pdo = new PDO('mysql:host=localhost;dbname=masterp1_journal;', $username, $password);      
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$email="";
$tasksleo = "";
$tasksKesho ="";

if(isset($_GET['email'])):
    $email = filter_var($_GET["email"], FILTER_SANITIZE_EMAIL);
    $user=  explode("@", $email);
    $user = $user[0];
    $stmt = $pdo->prepare("SELECT fullnames FROM user WHERE email=?");
    $stmt->execute(array($email));
    $rows = $stmt->rowCount();

    if($rows<1)
    {
        $stmt = $pdo->prepare('INSERT INTO user(fullnames,email) VALUES(?,?)');
        $stmt->execute(array( $user,$email ));      
    }
    else
    {
        $user = $stmt->fetchColumn();
        $date = date("Y-m-d", strtotime( '-1 days' ));
        $stmt = $pdo->prepare("SELECT tskLeo,tskKesho FROM activity INNER JOIN user ON activity.userid = user.id WHERE email =? AND date =?");
        $stmt->execute(array($email,$date));
        $rows = $stmt->rowCount();
        if($rows>0){
            $i=1;
           while($row=$stmt->fetch(PDO::FETCH_ASSOC))
            {            
                $tasksleo = $tasksleo.'</br>'.$i.'. '. $row['tskLeo'];
                $tasksKesho = $tasksKesho.'</br>'.$i.'. '. $row['tskKesho'];
                $i++;
            }       
        }        
}
endif;
//martin@masterpiecefusion.onmicrosoft.com Masterpiece2015
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Daily Journal</title>
    <meta name="description" content="Work daily journal" />
    <meta name="keywords" content="Keywords" />
    <meta name="author" content="Masterpiece Fusion" />
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    <style>
    hr {
        border-color: #808080;
        margin-top:5px;
    }
    .box_header:after {
        display: block;
        width: 50px;
        height: 2px;
        background: #42B3E5;
        margin-top: 10px;
        content: ""
    }
    </style>
</head>
<body>
    <div class="container">
        <h2></h2>
        <div><span class="text-primary h2">Daily Journal </span><span  style="float:right"><b>Date:</b> <?php echo date('d-m-Y');?></span></div>       
        <hr />
        <div class="row">
            <div class="col-md-6">
                <div class="row"><div class="col-md-12 text-danger">All fields are mandatory,Kindly List the items</div></div>
                <form class="form-horizontal" id="frmjournal">
                    <input id="rowid" name="rowid" type="hidden" >
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="email">Email</label>
                        <div class="col-md-6">
                            <input id="email" name="email" value="<?php echo $email;?>" type="email" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group" id="grp">
                        <label class="col-md-2 control-label" for="tskLeo">Today's Tasks</label>
                        <div class="col-md-8">
                            <textarea id="tskLeo[]" name="tskLeo[]" class="tskLeo" cols="50" ></textarea>
                        </div>
                        <div class="col-md-2">
                            <i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="Add another task" id="addLeo"></i>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-2 control-label" for="tskKesho">Tomorrow's Tasks</label>
                        <div class="col-md-8">
                            <textarea id="tskKesho[]" name="tskKesho[]" cols="50"></textarea>
                        </div>
                        <div class="col-md-2">
                            <i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="Add another task" id="addKesho"></i>
                        </div>
                    </div>

            


                    <div class="form-group">
                        <div class="col-md-10 col-lg-offset-2">
                            <button id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-success">Submit</button>
                            <button id="btnCancel" name="btnCancel" type="reset" class="btn btn-primary">Cancel</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-6" style="background-color:#c4bebe" id="preview">
                <h4 class="box_header">
                    <span>Preview Pane</span>
                </h4>
                <div class="row">
                    <div class="col-md-12" id="names"><b>Yesterday's tasks : </b><?php echo $tasksleo ;?></div>                    
                </div>
                <div class="row">
                    <div class="col-md-12" id="tasksyesterday"><b>Today's tasks : </b><?php echo $tasksKesho ;?></div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="taskstoday"></div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6">
                            <button id="btnEdit" name="btnEdit" type="submit" class="hidden btn btn-primary">Edit</button>                            
                        </div>
                    </div>
                </div>               
                <div class="col-xs-12" style="height:80px;"></div>
            </div>
        </div>


    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        // var tskLeoRule="";
        

         $('form#frmjournal').on('submit', function(event) {

            // adding rules for inputs with class 'comment'
            $('.tskKesho').each(function() {
                $(this).rules("add", 
                    {
                        required: true
                    })
            });
            $('.tskLeo').each(function () {
                $(this).rules("add", {
                    required: true
                });
             });            

            // prevent default submit action         
            event.preventDefault();

        })

        //form validation rules
        $("#frmjournal").validate({
            rules: {
                fullnames:{ required: true,
                    minlength: 4
                },
                "tskLeo[0]": {
                    required: true,
                    minlength: 4
                },

                "tskKesho[0]": {
                    required: true,
                    minlength: 4
                },
                job: {
                    required: true,
                },
            },


            submitHandler: function () {
                $.ajax({
                    type: 'POST',
                    url: 'taskPost.php',
                    data: $('form#frmjournal').serialize(),
                    success: function (data) {
                        var data = JSON.parse(data);

                        //toast notification
                        var t = new Toast(data.type, 'toast-top-right', data.msg);
                        toastr.options.positionClass = t.css;
                        toastr[t.type](t.msg);

                        if(data.type == "success"){
                            $("#btnEdit").removeClass('hidden');
                            $('#names').html('<b>Name:</b> ' + data.fullnames);
                            $('#tasksyesterday').html("<b>Yesterday's Tasks:</b></br> " + data.tskKesho.join('</br>'));

                            $('#taskstoday').html("<b>Today's Tasks:</b></br>  " + data.tskLeo.join('</br>'));
                            $('#rowid').val(data.rowid);

                            if (data.hasOwnProperty('update')) {
                                $("input[type=text], textarea").val(""); //clear fields as is update
                                $("#btnUpdate").replaceWith('<button id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-success">Submit</button>');
                            }
                            setTimeout(function () {
                                 $.ajax({
                                    type: 'POST',
                                    url: 'mail.php',
                                    data: {'userid':data.userid,'email':data.email},
                                    success: function (d) {
                                        var d = JSON.parse(d);
                                        toastr[d.type](d.text);
                                        },
                                    error: function (xhr, textStatus, error) {
                                        toastr['error'](error);

                                    }
                                });
                             }, 120000);

                        }else
                        {   //empty fields as input has error
                            $("input[type=text], textarea").val(""); 
                        }
                        
                        

                    },
                    error: function (xhr, textStatus, error) {
                        console.log('statusText' + xhr.statusText);
                        console.log('textStatus' + textStatus);
                        console.log('error' + error);
                        toastr['error'](error);

                    }
                });
}
});


        $("#btnEdit").click(function () {
            $("#btnSubmit").replaceWith('<button id="btnUpdate" name="btnUpdate" type="submit" class="btn btn-success">Update</button>');
            $("#email").focus();

        });       

        //toast function
        function Toast(type, css, msg) {
           this.type = type;
           this.css = css;
           this.msg = msg;
       }
       var i = 1;   
       $('#addLeo').click(function (){  
                         
            if(i<10){
              var row = "<div class='form-group'>  <div class='col-md-8 col-md-offset-2'><textarea id='tskLeo[]' name='tskLeo[]' cols='50' class='tskLeo'></textarea></div></div>";  
              $(this).parents('div.form-group').after(row);
              i++; 
             return false; 
            }        

       });
       var j = 1;
       $('#addKesho').click(function (){        
            
            if(j<10){
              var row = "<div class='form-group'>  <div class='col-md-8 col-md-offset-2'><textarea id='tskKesho[]' name='tskKesho[]' cols='50' required></textarea></div></div>";
              $(this).parents('div.form-group').after(row);
              j++; 
             return false; 
            }            

       });

   }); 
</script>
</body>
</html>
